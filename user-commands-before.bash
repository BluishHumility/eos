#!/bin/bash
#----------------------------------------------------------------------------------

_IsoConfig() {
    # ISO configurations here.
    # This runs before calamares is started.

    local -r install_mode="$1"          # 'online' or 'offline'
    
    # Define the subvolume name
    subvolume_name="value_of_subvolume_name"
    		
	# Use the new subvolume name, set mount options
	sed -i -e "s|@home|${subvolume_name}_home|" \
	       -e "s|@cache|${subvolume_name}_cache|" \
	       -e "s|@log|${subvolume_name}_log|" \
	       -e "s|@|${subvolume_name}|" \
	       -e "s|defaults, compress=zstd:1|noatime, compress=zstd|g" /etc/calamares/modules/mount.conf
}

## Execute the commands if the parameter list is valid:

case "$1" in
    offline | online) _IsoConfig "$1" ;;
esac
