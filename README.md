## What is this?

This is a repo for deploying a customized EndeavourOS installation. It leverages the `user_commands.bash` feature of EndeavourOS to achieve a pre-configured Sway setup. You can learn more about the `user_commands.bash` feature here: https://discovery.endeavouros.com/installation/customizing-the-endeavouros-install-process/

>>>
Note: this setup routine is designed for installing EndeavourOS in Btrfs subvolumes on an existing Btrfs partition. The disk is not meant to be formatted during this process. See this article for example: https://wiki.garudalinux.org/en/Multiple_installations_on_one_partition Or see this topic for a somewhat exaggerated example of this concept: https://forum.garudalinux.org/t/how-i-installed-every-garuda-spin-on-one-partition/27353
>>>

##  How does it work?

* Clone the repo into the EndeavourOS live environment and navigate inside it;
* Run the `unpack.sh` script. This script is used for setting some variables in the `user_commands.bash` script.
* Run the Calamares installer.
  * Choose "no desktop" for the DE choice;
  * Choose "no bootloader" for the bootloader choice;
  * Choose "manual partitioning". Mount EFI at `/efi`, mount Btrfs at `/`. Leave "Content" on "keep" for both partitions--*do not format* or you lose the base Arch installation.

## Why did you make this?

It makes installing EOS Sway with the exact configuration I prefer very fast and easy, which is useful for testing or other purposes. I can set up a new installation in a few minutes, make whatever changes I want, and then delete the subvolumes afterward if I wish, and no significant configuration effort has been lost.

## Can I use it?

No, not directly. This script is meant to be deployed on top of a Btrfs partition with a specific existing configuration (which comes from [this installation process](https://gitlab.com/BluishHumility/arch-i3)). Even if you had that same base configuration, it is unlikely you would want exactly the setup that this installation repo provides; it is a highly opinionated Sway setup with some configuration details that are specific to my personal preference and use case.

I have made it an open repo anyway in case anyone would like to take a look, and perhaps incorporate some of the ideas here into their own `user_commands.bash` script.
