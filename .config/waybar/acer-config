{
    "layer": "top",
    "position": "top", // Waybar position (top|bottom|left|right)
    // "height": 10,
    "output": "Acer Technologies EB321HQU C 0x00000848",
    "width": 2540,
    "spacing": 16, // Gaps between modules (in px)

// Configuration - modules-left

    "modules-left": [
        "custom/launcher",
        "sway/workspaces",
        "sway/mode",
        "sway/window"
    ],

    "custom/launcher": {
        "format":"<span size='x-large'></span>",
        "on-click": "exec nwg-drawer",
        "tooltip": false
    },

    "sway/workspaces": {
        "disable-scroll": true,
        "all-outputs": true,
        "format": "{icon}",
        "format-icons": {
            "3": "3",
            "4": "4",
            "5": "5",
            "6": "6",
            "7": "7",
            "8": "8",
            "9": "9",
            "10": "10"
        }
    },

    "sway/mode": {
        "format": "<span style=\"italic\">{}</span>",
        "tooltip": false
    },

    "sway/window": {
        "format": "{}",
        //"all-outputs": true, // Active window shows only on active display when commented
        "max-length": 120
    },

// Configuration - modules-center
    
    "modules-center": [
        "network"
    ],

    "network": {
        "format-disabled": " Disabled",
        "format-wifi": " {bandwidthDownBits:>} 󰶡 {bandwidthUpBits:>} 󰶣",
        "tooltip-format-wifi": "{essid}",
        "format-ethernet": "󰈀 {bandwidthDownBits:>} 󰶡 {bandwidthUpBits:>} 󰶣",
        "tooltip-format-ethernet": "{ifname}: {ipaddr}/{cidr}",
        "format-disconnected": " Disconnected",
        "on-click": "footclient -T waybar_nmtui -e nmtui",
        "interval": 2
    },

// Configuration - modules-right

    "modules-right": [
        "custom/updates",
        "idle_inhibitor",
        "cpu",
        "temperature",
        "memory",
        "backlight",
        "pulseaudio",
        "battery",
        "tray",
        "custom/notification",
        "clock",
        "custom/power"
    ],

    "custom/updates": {
        "format": "{} {icon} ",
        "return-type": "json",
        "format-icons": {
            "has-updates": "",
            "updated": ""
        },
        "exec-if": "which waybar-module-pacman-updates",
        "exec": "waybar-module-pacman-updates",
        "on-click": "footclient -T waybar_update -e bash -c 'eos-update && (read -p \"Update complete. Press Enter to exit.\" && exit 0) || (read -p \"Update failed. Press Enter to exit.\" && exit 1)'",
        "on-click-right": "footclient -T waybar_pacseek -e pacseek"
    },

    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": " ",
            "deactivated": " "
        },
        "tooltip-format-activated": "Idle Inhibitor Activated",
        "tooltip-format-deactivated": "Idle Inhibitor Deactivated"
    },

    "cpu": {
        "interval": 5,
        "format": "{usage:>3}%",
        "states": {
            "warning": 70,
            "critical": 90,
        },
        "on-click": "footclient -T waybar_btm -e btm"
    },
    
    "memory": {
        "interval": 5,
        "format": "{:>3}%",
        "on-click": "footclient -T waybar_btm -e btm", 
        "states": {
            "warning": 70,
            "critical": 90
        }
    },
    "temperature": {
        "critical-threshold": 80,
        "format-critical": " {temperatureC}°C",
        "format": " {temperatureC}°C",
        "tooltip-format": "{temperatureC}° Celsius\n{temperatureF}° Fahrenheit\n{temperatureK}° Kelvin",
        "on-click": "footclient -T waybar_btm -e btm"
    },    

    "backlight": {
        "format": "{icon} {percent}%",
        "format-icons": ["󰄰", "󰪞", "󰪟", "󰪠", "󰪡", "󰪢", "󰪣", "󰪤", "󰪥"],
        "tooltip-format": "Backlight (Scroll): {percent:}%",
        "on-scroll-down": "brightnessctl -c backlight set 5%-",
        "on-scroll-up": "brightnessctl -c backlight set +5%"
    },

    "pulseaudio": {
        "scroll-step": 2,
        "format": "{icon} {volume}%",
        "format-muted":"󰝟 muted",
        "format-icons": {
            "headphones": "",
            "handsfree": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["󰕿", "󰖀", "󰕾"]
        },
        "on-click": "footclient -T waybar_alsamixer -e alsamixer -M",
        "on-click-right": "pavucontrol"
    },

    "battery": {
        "states": {
            "warning": 30,
            "critical": 15
        },
        "format": "{icon} {capacity}%",
        "format-icons": ["", "", "", "", ""]
    },

    "tray": {
        "icon-size": 16,
        "spacing":10
    },

    "custom/notification": {
        "tooltip": false,
        "format": "{icon} ",
        "format-icons": {
            "notification": "<span foreground='red'><sup></sup></span>",
            "none": "",
            "dnd-notification": "<span foreground='red'><sup></sup></span>",
            "dnd-none": "",
            "inhibited-notification": "<span foreground='red'><sup></sup></span>",
            "inhibited-none": "",
            "dnd-inhibited-notification": "<span foreground='red'><sup></sup></span>",
            "dnd-inhibited-none": ""
        },
        "return-type": "json",
        "exec-if": "which swaync-client",
        "exec": "swaync-client -swb",
        "on-click": "swaync-client -t -sw",
        "on-click-right": "swaync-client -d -sw",
        "escape": true
    },

    "clock": {
        "format": "󰅐 {:%OI:%M %p}",
        "on-click": "footclient -T waybar_calcurse -e calcurse",
        "on-click-right": "notify-send \"Running calcurse-caldav...\" \"$(calcurse-caldav)\"",
        "tooltip-format": " {:%A %m/%d}\n\n<tt><small>{calendar}</small></tt>",
        "calendar": {
            "on-scroll": 1,
            "format": {
                "months":     "<span color='#ffead3'><b>{}</b></span>",
                "days":       "<span color='#ecc6d9'><b>{}</b></span>",
                "weeks":      "<span color='#99ffdd'><b>W{}</b></span>",
                "weekdays":   "<span color='#ffcc66'><b>{}</b></span>",
                "today":      "<span color='#ff6699'><b><u>{}</u></b></span>"
            },
        },
        "actions": {
            "on-scroll-up": "shift_up",
            "on-scroll-down": "shift_down"
        },
    },



    "custom/power": {
        "format":"⏻",
        "on-click": "exec ~/.config/sway/scripts/power_menu.sh",
        "tooltip": false
    },
}
