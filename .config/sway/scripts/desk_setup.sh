#!/bin/bash

# Check if my desk outputs are active
if swaymsg -t get_outputs | grep -q 'EB321HQU C' ||
  swaymsg -t get_outputs | grep -q 'S22E450'; then
  # If they are, use my desk setup
  swaymsg output eDP-1 pos 0 0 scale 1.5
  swaymsg output '"Acer Technologies EB321HQU C 0x00000848"' pos 1504 0
  swaymsg output '"Samsung Electric Company S22E450 HCHN502280"' transform 270 pos 4064 0 bg "#000000" solid_color
  pkill waybar
  waybar -c ~/.config/waybar/acer-config &
else
  pkill -f "waybar -c /home/$USER/.config/waybar/acer-config"
fi
