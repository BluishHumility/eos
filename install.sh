#!/bin/sh

echo "Starting the custom EOS install script..."

# Establish if encryption will be set up
while true; do
    read -p "Is the root partition encrypted? (y/n): " luks_install
    if [[ "$luks_install" == "y" || "$luks_install" == "n" ]]; then
        break  # Exit the loop if the input is "y" or "n"
    else
        echo "Invalid input. Please enter 'y' or 'n'."
    fi
done

# Confirm encryption choice
if [[ "$luks_install" == "y" ]]; then
    echo "Proceeding with encrypted install."
else
    echo "Proceeding with non-encrypted install."
fi

# Replace the luks_install placeholder value in user_commands.bash
sed -i "s|luks_install_placeholder|\"${luks_install}\"|" user_commands.bash

# Define the subvolume name
read -p "Enter subvolume name: " subvolume_name
# Replace the subvolume placeholder value
sed -i "s|value_of_subvolume_name|\"${subvolume_name}\"|g" user-commands-before.bash
sed -i "s|value_of_subvolume_name|\"${subvolume_name}\"|g" user_commands.bash

# Collect GPG passphrase
while true; do
    # Get the passphrase
    read -rsp "Enter passphrase for curtain tarball: " gpg_passphrase
    echo

    # Confirmation passphrase
    read -rsp "Confirm passphrase: " gpg_passphrase_confirm
    echo

    # Check if passphrases match
    if [ "$gpg_passphrase" = "$gpg_passphrase_confirm" ]; then
        break  # Passphrases match, break the loop
    else
        echo "The confirmation passphrase did not match--try again."
    fi
done

# Replace the GPG passphrase placeholder value
sed -i "s|gpg_passphrase_placeholder|\"${gpg_passphrase}\"|" user_commands.bash

echo "Passphrase set successfully."

# Identify the root partition for LUKS installations
if [[ "$luks_install" == "y" ]]; then
    # List available disks
    lsblk -o name,type,fstype,label,size

    # Identify boot partition
    while true; do
        # Ask the user to select a partition
        read -p "Identify the boot partition (e.g., sda2): " boot_partition
        echo

        # Verify disk selection
        read -p "Confirm /dev/$boot_partition is the boot partition: (y/n): " choice
        echo

        # Check for confirmation
        if [[ "$choice" =~ ^[Yy]$ ]]; then
            break
        else
            echo "Not confirmed."
        fi
    done

    # Replace the boot partition placeholder value
    sed -i "s|boot_partition_placeholder|/dev/\"${boot_partition}\"|" user_commands.bash

    # Identify encrypted Btrfs partition
    while true; do
        # Ask the user to select a partition
        read -p "Identify the encrypted Btrfs partition (e.g., sda3): " btrfs_partition
        echo

        # Verify disk selection
        read -p "Confirm /dev/$btrfs_partition is the encrypted Btrfs partition: (y/n): " choice
        echo

        # Check for confirmation
        if [[ "$choice" =~ ^[Yy]$ ]]; then
            break
        else
            echo "Not confirmed."
        fi
    done

    # Open the LUKS device
    # Use "live-[foobar]" for mapping name so Calamares won't close it
    # See this related thread: https://github.com/calamares/calamares/issues/1564
    sudo cryptsetup open "/dev/${btrfs_partition}" live-root
fi

# Move user-commands-before.bash and user_commands.bash out to the home dir
echo "Moving user-commands-before.bash and user_commands.bash to user home directory..."
mv user-commands-before.bash ../user-commands-before.bash
mv user_commands.bash ../user_commands.bash

# Edit /usr/bin/eos-install-mode-run-calamares so it does not launch Calamares in the background
echo "Editing eos-install-mode-run-calamares..."
sudo sed -i 's|>> $log &|>> $log|' /usr/bin/eos-install-mode-run-calamares

# Launch /usr/bin/eos-install-mode-run-calamares
echo "Launching the installer..."
/usr/bin/eos-install-mode-run-calamares

# Calamares does not currently support creating subvolumes on partitions other than /
# See this related topic: https://forum.endeavouros.com/t/calamares-not-using-boot-subvolume-user-commands-bash/54048/3?u=bluishhumility
# As a workaround, set this up manually post-install for encrypted setups
if [[ "$luks_install" == "y" ]]; then
    # Mount the boot partition
    echo "Mounting the boot partition..."
    sudo mount -o subvolid=0 "/dev/${boot_partition}" /mnt
    
    # Create the boot subvolume
    echo "Creating the boot subvolume..."
    sudo btrfs subvolume create "/mnt/${subvolume_name}_boot"

    # Move the kernels, images, and microcode to the boot subvolume
    echo "Moving kernels, images, and microcode to the boot subvolume..."
    sudo mv /mnt/initramfs* /mnt/vmlinuz* /mnt/*ucode* "/mnt/${subvolume_name}_boot/"

    # Unmount the boot partition
    echo "Unmounting the boot partition..."
    sudo umount /mnt
fi

# Installation complete
echo "Installation complete."
