#!/bin/bash
#----------------------------------------------------------------------------------

_PostInstallCommands() {
    # Post-install commands here.
    # This runs near the end of calamares execution.

    # New user created for the target
    local -r username="$1"
    
    # Define the subvolume name
    subvolume_name="value_of_subvolume_name"

    # Set luks_install variable
    luks_install="luks_install_placeholder"

    # Get the UUID of the root device
    if [[ "$luks_install" == "y" ]]; then
        # Get the UUID of the Btrfs crypt device
        btrfs_uuid=$(blkid -s UUID -o value /dev/mapper/live-root)
        # Replace /dev/mapper/live-root with UUID in fstab
        echo "Replacing /dev/mapper/live-root with UUID in fstab..."
        sed -i "s|/dev/mapper/live-root|UUID=${btrfs_uuid}|g" /etc/fstab
        # Add subvolume to /boot entry in fstab
        sed -i "/\/boot/{s|noatime|subvol=${subvolume_name}_boot,noatime|}" /etc/fstab
    else
        # Get the UUID of the Btrfs partition
        btrfs_uuid=$(findmnt -no UUID /)
    fi

    # Add fstab entry for btrbk subvolume
    echo "Adding fstab entry for btrbk subvolume..."
    # Create the mount point
    mkdir -p /_btrbk_snap
    # Add the fstab entry
    echo "UUID=$btrfs_uuid /_btrbk_snap btrfs subvol=/btrbk_snap,noatime,compress=zstd 0 0" | tee -a /etc/fstab

    # Add fstab entry for shared subvolume
    echo "Adding fstab entry for shared subvolume..."
    # Create the mount point
    mkdir -p /share
    # Restore ownership to the user
    chown -R $username:$username /share
    # Add the fstab entry
    echo "UUID=$btrfs_uuid /share btrfs subvol=/share,noatime,compress=zstd 0 0" | tee -a /etc/fstab

    # Array containing paths to resources
    declare -A resources
    resources[ssh]=".ssh"
    resources[librewolf]=".librewolf"
    resources[applications]="Applications"
    resources[documents]="Documents"
    resources[downloads]="Downloads"
    resources[git]="Git"
    resources[music]="Music"
    resources[pictures]="Pictures"
    resources[sync]="Sync"
    resources[videos]="Videos"
    resources[signal]=".config/Signal"
    resources[joplin]=".config/Joplin"
    resources[joplin-desktop]=".config/joplin-desktop"
    resources[onlyoffice-config]=".config/onlyoffice"
    resources[onlyoffice-local]=".local/share/onlyoffice"
    resources[syncthing]=".local/state/syncthing"

    # Build the fstab entries
    > ~/temp_fstab.txt
    for resource in "${!resources[@]}"; do
      mount_point="/home/${username}/${resources[$resource]}"
      subvol="/share/${resources[$resource]}"
      printf 'UUID=%s %s btrfs subvol=%s,noatime,compress=zstd 0 0\n' "$btrfs_uuid" "$mount_point" "$subvol" >> ~/temp_fstab.txt
    done
    
    # Append to /etc/fstab
    echo "Updating /etc/fstab..."
    if ! diff -q /etc/fstab ~/temp_fstab.txt >/dev/null; then
      cat ~/temp_fstab.txt >> /etc/fstab
      rm ~/temp_fstab.txt
    fi

    # Identify the LUKS device for encrypted setups
    if [[ "$luks_install" == "y" ]]; then
        crypt_block_device=$(cryptsetup status live-root | awk '$1 == "device:" {print $2}')
        cryptdevice_uuid=$(blkid -s UUID -o value "${crypt_block_device}")
    fi

    # Write a rEFInd boot stanza
    echo "Generating a rEFInd boot stanza..."
    if [[ "$luks_install" == "y" ]]; then
        cat <<EOF >> /efi/EFI/refind/additional_stanzas.conf
menuentry ${subvolume_name} {
	icon 	/EFI/refind/eos-sway.png
	volume 	boot
	loader 	/${subvolume_name}_boot/vmlinuz-linux
	initrd 	/${subvolume_name}_boot/initramfs-linux.img
	graphics on
	options "cryptdevice=UUID=${cryptdevice_uuid}:live-root root=/dev/mapper/live-root rw rootflags=subvol=${subvolume_name}"
	submenuentry "Fallback initramfs" {
		initrd	/${subvolume_name}_boot/initramfs-linux-fallback.img
	}
	submenuentry "LTS kernel" {
		loader	${subvolume_name}_boot/vmlinuz-linux-lts
		initrd	${subvolume_name}_boot/initramfs-linux-lts.img
	}
	submenuentry "LTS fallback" {
		loader	${subvolume_name}_boot/vmlinuz-linux-lts
		initrd	${subvolume_name}_boot/initramfs-linux-lts-fallback.img
	}		
}

EOF
    else
        cat <<EOF >> /efi/EFI/refind/additional_stanzas.conf
menuentry ${subvolume_name} {
	icon 	/EFI/refind/eos-sway.png
	volume 	Btrfs
	loader 	/${subvolume_name}/boot/vmlinuz-linux
	initrd 	/${subvolume_name}/boot/initramfs-linux.img
	graphics on
	options "root=UUID=${btrfs_uuid} rw rootflags=subvol=${subvolume_name}"
	submenuentry "Fallback initramfs" {
		initrd	/${subvolume_name}/boot/initramfs-linux-fallback.img
	}
	submenuentry "LTS kernel" {
		loader	${subvolume_name}/boot/vmlinuz-linux-lts
		initrd	${subvolume_name}/boot/initramfs-linux-lts.img
	}
	submenuentry "LTS fallback" {
		loader	${subvolume_name}/boot/vmlinuz-linux-lts
		initrd	${subvolume_name}/boot/initramfs-linux-lts-fallback.img
	}		
}

EOF
    fi
 
    # Clone the startup repo
    echo "Cloning startup repo..."
    git clone https://gitlab.com/BluishHumility/eos-sway.git

    # Clone the common-configs repo
    echo "Cloning common-configs repo..."
    git clone https://gitlab.com/BluishHumility/common-configs.git
    
    # Add the GPG passphrase to a file
    echo "gpg_passphrase_placeholder" > passphrase.txt
    # Decrypt the archive
    echo "Decrypting archive..."
    gpg --batch --passphrase-file passphrase.txt -o ./eos-sway/curtain.tar.gz -d ./eos-sway/curtain.tar.gz.gpg
    # Extract the archive
    echo "Extracting archive..."
    tar xzf ./eos-sway/curtain.tar.gz
    # Distribute the curtain files
    rsync -a ./curtain/.config/ ./eos-sway/.config/
    rsync -a --exclude='*/' ./curtain/ "/home/${username}/"
    
    # Package installation
    echo "Adding Chaotic repo..."
    pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
	pacman-key --lsign-key 3056513887B78AEB
	pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
	echo -e "\n[chaotic-aur]\nInclude = /etc/pacman.d/chaotic-mirrorlist" >> /etc/pacman.conf
    pacman -Sy
    echo "Installing user-defined package list..."
    pacman -S --noconfirm --noprogressbar --needed --disable-download-timeout $(< ./eos-sway/packages.list)

    # Set up AppImage symlinks
    echo "Setting up AppImage symlinks..."
    ln -s "/home/${username}/Applications/LibreWolf.AppImage" /usr/local/bin/librewolf
    ln -s "/home/${username}/Applications/Joplin.AppImage" /usr/local/bin/joplin
    ln -s "/home/${username}/Applications/OnlyOffice.AppImage" /usr/local/bin/onlyoffice
    #ln -s "/home/${username}/Applications/GIMP.AppImage" /usr/local/bin/gimp
    ln -s "/home/${username}/Applications/Krita.AppImage" /usr/local/bin/krita
    ln -s "/home/${username}/Applications/darktable.AppImage" /usr/local/bin/darktable
    ln -s "/home/${username}/Applications/Upscayl.AppImage" /usr/local/bin/upscayl
    
    # Deploy user configs
    echo "Deploying user configs..."
    rsync -a common-configs/.config "/home/${username}/"
    rsync -a eos-sway/.config "/home/${username}/"
    rsync -a common-configs/.local "/home/${username}/"
    rsync -a eos-sway/.local "/home/${username}/"
    # Restore user ownership
    chown -R "${username}:${username}" "/home/${username}"

    # A few configs I like root to have
    root_configs=(
    	"fish/"
    	"Kvantum/"
    	"lf/"
    	"micro/"
    )
    # Copy the configs over to the root user
    mkdir -p /root/.config/
    for root_config in "${root_configs[@]}"; do
        cp -R "/home/${username}/.config/$root_config" /root/.config/
    done

    # Create btrbk directories
    mkdir -p /_btrbk_snap/{Documents,Music,Pictures,Sync,Videos,"$subvolume_name"}
    
    # Deploy system configs
    echo "Deploying system configs..."
	# Copy system files and restore root ownership
	rsync -a --chown=root:root common-configs/etc/ /etc/
	rsync -a --chown=root:root eos-sway/etc/ /etc/
	rsync -a --chown=root:root common-configs/usr/ /usr/
	# Remove the repos and configs
	echo "Removing installation repos and configs..."
	rm -rf eos-sway common-configs curtain passphrase.txt

	# Generate Sway wrapper script
    echo "Generating Sway wrapper script..."
    cat <<'EOF' > /usr/local/bin/sway-run
#!/bin/bash

export XDG_SESSION_TYPE=wayland
export XDG_CURRENT_DESKTOP=sway
export XDG_SESSION_DESKTOP=sway
export QT_QPA_PLATFORM=wayland
export QT_QPA_PLATFORMTHEME=gtk3
export QT_STYLE_OVERRIDE=kvantum
export GTK_THEME=Nordic-darker
export EDITOR=micro
export BROWSER=librewolf
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

sway
EOF
    # Apply the executable attribute
    chmod +x /usr/local/bin/sway-run

    # Create Snapper config
    echo "Creating Snapper config for root..."
    snapper --no-dbus create-config --template custom_root /
    # If there is a boot partition, create a Snapper config for it
    if [[ "$luks_install" == "y" ]]; then
        echo "Creating Snapper config for boot..."
        snapper --no-dbus -c boot create-config --template custom_root /boot
        # Add boot config to snap-pac.ini
        echo "Adding boot config to snap-pac.ini..."
        cat <<EOF >> /etc/snap-pac.ini

[boot]
snapshot = True
EOF
    fi

    # Correct whoogle-search directory ownership
    echo "Correcting whoogle-search directory ownership..."
    chown -R whoogle:whoogle /opt/whoogle-search/
    
    # Enable systemd services
    echo "Enabling systemd services..."
    systemctl enable btrbk-snapshot.timer
    systemctl enable snapper-cleanup.timer
    systemctl enable tailscaled.service
    systemctl enable whoogle.service
    systemctl enable greetd.service
    # Add the greetd drop-in to prevent journal output on the tuigreet screen
    mkdir -p /etc/systemd/system/greetd.service.d/
    cat <<EOF > /etc/systemd/system/greetd.service.d/override.conf
[Unit]
After=multi-user.target

[Service]
Type=idle

EOF
    
    # Enable systemd user services
    echo "Enabling systemd user service..."
    sudo -u $username systemctl --user enable ssh-agent.service
    sudo -u $username systemctl --user enable btrbk-backup-reminder.service
        
    # Add the MIT mirror to /etc/pacman.conf
    echo "Adding MIT mirror to /etc/pacman.conf..."
    server_url="https://mirrors.mit.edu/archlinux/\$repo/os/\$arch"
    sed -i "s|Include = /etc/pacman.d/mirrorlist|Server = $server_url\nInclude = /etc/pacman.d/mirrorlist|g" /etc/pacman.conf

    # Enable magic SysRq
    echo "Enabling magic SysRq..."
    echo 'kernel.sysrq=1' | sudo tee /etc/sysctl.d/99-reisub.conf
    
}

## Execute the commands if the parameter list is valid:

case "$1" in
    --iso-conf* | online | offline | community) ;;   # no more supported here
    *) _PostInstallCommands "$1" ;;
esac
